const { io } = require('../index');
const Band = require('../models/band');
const Bands = require('../models/bands');

const bands = new Bands();
bands.addBand(new Band('Iron Maiden'));
bands.addBand(new Band('Foo Fighters'));
bands.addBand(new Band('Heroes del Silencio'));
bands.addBand(new Band('Daft Punk'));

io.on('connection', client => {
    console.log("Cliente conectado.");

    // Cuando el cliente se conecta emitir el evento
    // que envía las bandas activas solo a él.
    client.emit('active-bands', { bands: bands.getBands() });

    client.on('disconnect', () => {
        console.log("Cliente desconectado.");
    });

    client.on('message', (payload) => {
        io.emit('message', { message: `Mensaje recibido. Hola ${payload.name}` });
    });

    // Cuando un cliente vota por una banda se guarda el voto y
    // se envía el evento de bandas activas a todos los clientes.
    client.on('vote-band', (payload) => {
        bands.voteBand(payload.id);
        io.emit('active-bands', { bands: bands.getBands() });
    });

    // Cuando un cliente agrega una banda se guarda y se envía
    // el evento de bandas activas a todos los clientes.
    client.on('add-band', (payload) => {
        bands.addBand(new Band(payload.name));
        io.emit('active-bands', { bands: bands.getBands() });
    });

    // Cuando un cliente borra una banda se guarda y se envía
    // el evento de bandas activas a todos los clientes.
    client.on('delete-band', (payload) => {
        bands.deleteBand(payload.id);
        io.emit('active-bands', { bands: bands.getBands() });
    });
});